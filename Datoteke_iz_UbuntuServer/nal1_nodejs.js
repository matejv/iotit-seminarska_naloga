const http = require("http");
const fs = require("fs"); // knjižnice za delo z datotekami, ki jih rabimo pri posredovanju HTML strani v datoteki primer03.html

const hostname = "192.168.1.144";
const port = 80;

const server = http.createServer((req, res) => {
    if (req.url === "/") {
        fs.readFile(__dirname + "/nal1_htmlstran.html", (err, data) => {
            if (err) { // če pri branju datoteke nal1_htmlstran.html pride do napake
                res.writeHead(500, {"Content-Type": "text/plain; charset=utf-8"});
                return res.end("Napaka pri nalaganju HTML strani. Branje datoteke nal1_htmlstran.html z diska ni bilo mogoče."); // klientu kot odgovor ("response") posredujemo obvestilo o napaki
            }
    
            res.writeHead(200);
            res.end(data);
        });
    }
});

server.listen(port, hostname, () => {
    console.log("Strežnik teče na http://" + hostname + ":" + port);
});
