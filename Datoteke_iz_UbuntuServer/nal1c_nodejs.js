/********************************************************************
* Za uporabo knjižnice Firmata mora biti nameščen Node.js verzije 12
* Ta primer je izveden z verzijo Node.js v12.13.0 (npm 6.12.0)
* Namestitev verzije Node.js 12.13.0: nvm install 12.13.0
* Preveri nameščeno verzijo Node.js: node -v
*********************************************************************/

var http = require("http");
var firmata = require("firmata");

var board = new firmata.Board("/dev/ttyACM0", function () { // ACM (Abstract Control Model) for serial communication with Arduino (could be USB)
  board.pinMode(13, board.MODES.OUTPUT); // Configures the specified pin to behave either as an input or an output.
});

const hostname = "192.168.1.144";
const port = 8080;

const server = http.createServer(function (req, res) { // http.createServer([requestListener]) | The requestListener is a function which is automatically added to the 'request' event.
  var parts = req.url.split("/"), // split request url on "/" character
    operator = parseInt(parts[1], 10); // 10 is radix - decimal notation; the base in mathematical numeral systems (from 2 to 36)

  if (operator == 0) {
    board.digitalWrite(13, board.LOW);
  }
  else if (operator == 1) {
    board.digitalWrite(13, board.HIGH);
  }

  res.writeHead(200, "<meta charset='UTF-8'>", "<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>");
  res.write("<table style='margin-left: auto; margin-right: auto; font-family: Helvetica; cursor: pointer; border-radius: 4px; border: none;'>");
  res.write("<tr style='background-color: #3499db; font-size: 25px; display: block; padding: 13px 30px;'>");
  res.write("<td style='width: 200px; text-align: left;'><a style='color: white; text-decoration: none;' href='http://192.168.1.144:8080/1'>Vklopi LED</a></td>")
  res.write("</tr");
  res.write("<tr>")
  res.write("<td style='width: 200px; text-align: right;'><a style='color: white; text-decoration: none;' href='http://192.168.1.144:8080/0'>Izklopi LED</a></td>");
  res.write("</tr>");
  res.write("<tr style='text-align: center; font-size: 20px; color: #222222;'>");
  res.write("<td colspan='2'>Vrednost LED je: <b>" + operator + "</b></td>");
  res.write("</tr>");
  res.write("<tr style='text-align: center; font-size: 16px; color: #444444;'>");
  res.write("<td colspan='2'>(1 = vklopljeno | 0 = izklopljeno)</td>");
  res.write("</tr>");
  res.write("</table>");
  res.end();
});

server.listen(port, hostname, () => {
  console.log("Strežnik teče na http://" + hostname + ":" + port);
});