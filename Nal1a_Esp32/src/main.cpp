#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WebSocketsServer.h>

const char* ssid = "Omrezje_Matej_Vovko";
const char* password = "geslo12345";

// port 80 je prednastavljeni port za posredovanje HTML strani
WebServer server(80); // objekt razreda WebServer (HTTP port, 80 je prednastavljeni port oz. vrata)
                      // vrednost porta posredujemo kot argument konstruktorja

// na vratih 81 ustvarimo strežnik spletnih vtičnikov - WebSocketsServer
WebSocketsServer webSocket = WebSocketsServer(81);

uint8_t LED1pin = 2; // 8 bitna celoštevilska spremenljivka
bool LED1status = LOW;

// ***********************************************************
// Definicija funkcij glede na zahtevo uporabnika ("request")
// ***********************************************************
// tu določimo kaj se dogodi, če uporabnik vpiše
// korenski ("root") naslov našega strežnika na esp32 modulu, npr. http://192.168.1.115
void handle_root() { // če dobimo zahtevo na strežnik ("request")
  server.send(200); // strežnik klientu pošlje kot odziv, t. j. "response"
                                        // HTML0 v argumentu je spremenljivka tipa String
                                        // v njej je spravljena naša spletna stran
                                        // koda 200 predstavlja standardni odziv na uspešno HTTP zahtevo - OK
}

void handle_NotFound() { // v primeru, da ne najdemo spletne strani, npr. če vpišemo /abc
  server.send(200, "text/html", "Spletna stran ni bila najdena."); // klientu sporočimo, da zahtevane spletne strani nismo našli na našem strežniku
}

// *******************************************************************************************************
// Definicija funkcije onWebSocketEvent, ki se izvede kadarkoli dobimo sporočilo preko spletnega vtičnika
// preko WebSocketa (oz. se dogodi dogodek - "event" na spletnem vtičniku)
// *******************************************************************************************************
// num predstavlja številčenje oz. indetifikator klientov, če odpremo npr. 3x Chrome - imamo [0], [1], [2]
// na ta način lahko določimo za katerega klienta gre, če je istočasno priključenih več klientov na esp32
// type - različni tipi sporočil (WStype_DISCONNECTED, WStype_CONNECTED, WStype_TEXT itd.)
// payload je String, če npr. pošljemo s strani klienta povezava.send('abc') bo String((char) payload[0])='a'
// String((char) payload[1])='b', String((char) payload[2])='c'
// length je dolžina niza oz. Stringa; če je payload npr. 'abc' je dolžina, t.j. length enaka 3
void onWebSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
  // določimo funkcionalnost glede na tip dogodka na spletnem vtičniku (WebSocketu)
  switch (type)
  {
  case WStype_DISCONNECTED:
    {
      Serial.printf("[%u] Klient odklopljen!\n", num);
    }
    break;

  case WStype_CONNECTED:
    {
      IPAddress ip = webSocket.remoteIP(num);
      Serial.printf("[%u] Vzpostavljena WebSocket povezava iz IP naslova ", num);
      Serial.println(ip.toString());
    }
    break;

  case WStype_TEXT:
    {
      Serial.printf("[%u] Besedilo sporočila: %s\n", num, payload); // izpis sporočila v serijski monitor
      String besedilo = String((char) payload[0]);

      if (besedilo == "1") {
        digitalWrite(LED1pin, 1); // na nožici 2 je vgrajena LED dioda - zapis visoke vrednosti
      }

      if (besedilo == "0") {
        digitalWrite(LED1pin, 0); // na nožici 2 je vgrajena LED dioda - zapis nizke vrednosti
      }
    }
    break;
  
  default:
    break;
  } // KONEC switch stavka
}

void setup() {
  // put your setup code here, to run once:
  // tu zapišemo kodo za nastavitve, koda bo izvedena le enkrat:

  pinMode(LED1pin, OUTPUT);
  delay(100);

  // poženemo komunikacijo preko serijskega protokola
  Serial.begin(115200);

  // povežemo se z dostopno WiFi točko
  WiFi.begin(ssid, password); // WiFi omrežje poženemo

  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Povezovanje z WiFi omrežjem ...");
  }

  String ip = WiFi.localIP().toString();
  Serial.println("Povezava z WiFi omrežjem je vzpostavljena.");
  Serial.print("IP naslov esp32 modula je: ");
  Serial.println(ip);

  server.on("/", handle_root); // ko vpišemo IP naslov v brskalnik, pokličemo funkcijo handle_root, ki vrne kratko sporočilo klientu
  server.onNotFound(handle_NotFound); // v primeru, da spletne strani ni na strežniku izpišemo obvestilo, da strani ne najdemo
  server.begin(); // zagon HTTP strežnika
  Serial.print("HTTP strežnik je zagnan, vpišite IP naslov v brskalnik");

  // poženemo WebSocket strežnik in določimo "callback"
  webSocket.begin(); // zagon WebSocket strežnika
  webSocket.onEvent(onWebSocketEvent); // kadarkoli se bo dogodil določen dogodek preko spletnega vtičnika
                                       // WebSocket bomo izvedli funkcijo onWebSocketEvent, ki je definirana zgoraj

  delay(500);

  // del, ki služi kot indikator, da je koda prenesena na modul
  pinMode(2, OUTPUT); // nožica št. 2 bo delovala kot izhod
  digitalWrite(2, HIGH); // na nožico 2 zapišemo visoko vrednost - LED zasveti
  delay(750); // zakasnitev izvedbe programa za specificiran čas v milisekundah, 1000 ms = 1 s
  digitalWrite(2, LOW); // na nožico 2 zapišemo nizko vrednost - LED ugasne
  delay(750); // zakasnitev izvedbe programa za specificiran čas v milisekundah, 1000 ms = 1 s
  digitalWrite(2, HIGH); // na nožico 2 zapišemo visoko vrednost - LED zasveti
  delay(750); // zakasnitev izvedbe programa za specificiran čas v milisekundah, 1000 ms = 1 s
  digitalWrite(2, LOW); // na nožico 2 zapišemo nizko vrednost - LED ugasne
}

void loop() {
  // put your main code here, to run repeatedly:
  // tu zapišemo kodo, ki se ponavljajoče izvaja:
  server.handleClient(); // spremlja prisotnost klientov in posluša njihove HTML zahteve ("request")

  webSocket.loop(); // poslušamo na spletnem vtičniku - WebSocket za podatke in dogodke
}